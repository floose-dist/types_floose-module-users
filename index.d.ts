import {Framework} from "floose";
import ComponentConfiguration = Framework.ComponentConfiguration;
import Model = Framework.Data.Model;

export declare enum DriverType {
    MONGODB = "mongodb",
    MSSQL = "mssql",
    MYSQL = "mysql"
}
export declare enum AuthenticationType {
    PASSWORD = "password",
    REFRESH_TOKEN = "refresh_token",
    CLIENT_CREDENTIALS = "client_credentials"
}
export declare enum PermissionAction {
    ALLOW = "allow",
    DENY = "deny"
}
// Key used in Rest and Socket.io request artifacts
export declare enum ArtifactData {
    USER = "authenticated_user",
    SESSION = "authenticated_user_session"
}

/**
 * CONFIGURATION
 */
export interface UsersModuleConfig extends ComponentConfiguration {
    driver: DriverType;
    connection: string;
    authentication: {
        /**
         * allowed authentication types
         */
        types: AuthenticationType[];
        /**
         * tokens time-to-live
         * use 0 to set unlimited duration for tokens, so they will never expire
         */
        ttl?: {
            token: number;
            refreshToken?: number;
        }
    }
}

/**
 * MANAGERS
 */
export declare class SessionManager {
    protected constructor();
    static getInstance(): SessionManager;

    getSessionById(id: string | string[]): Promise<Session | Session[]>;
    getSessionByToken(token: string): Promise<Session>;
    getSessionByRefreshToken(refreshToken: string): Promise<Session>;
    createSession(user: User, token: string, refreshToken: string, device?: string, lastRequestIp?: string): Promise<Session>;
    deleteSession(session: Session | Session[]): Promise<void>;
}
export declare class UserManager {
    protected constructor();
    static getInstance(): UserManager;

    getUserById(id: string | string[]): Promise<User | User[]>;
    getUserByLogin(username: string, password?: string): Promise<User>;
    getUserByEmail(email: string): Promise<User>;
    getUserByMetadata(key: string, value: any): Promise<User[]>
    isUserAllowed(user: User, path: string): Promise<boolean>;
    createUser(username: string, password: string, email: string, metadata?: {[key:string]: any}, roles?: Role[], permissions?: Permission[]): Promise<User>;
    deleteUser(user: User | User[]): Promise<void>;
    updateUserById(id: string, username?: string, password?: string, email?: string, metadata?: {
        [key: string]: any;
    }): Promise<User>;

    getPermissionById(id: string | string[]): Promise<Permission | Permission[]>;
    getPermissionByCode(code: string | string[]): Promise<Permission | Permission[]>;
    getPermissionsByUser(user: User, includeRolePermissions?: boolean): Promise<Permission[]>;
    getPermissionsByRole(role: Role): Promise<Permission[]>;
    createPermission(code: string, path: string, action: PermissionAction): Promise<Permission>;
    addPermissionToRole(role: Role, permission: Permission): Promise<Role>;
    addPermissionToUser(user: User, permission: Permission): Promise<User>;
    removePermissionFromRole(role: Role, permission: Permission | Permission[]): Promise<Role>;
    removePermissionFromUser(user: User, permission: Permission | Permission[]): Promise<User>;
    deletePermission(permission: Permission | Permission[]): Promise<void>;

    getRoleById(id: string | string[]): Promise<Role | Role[]>;
    getRoleByCode(code: string | string[]): Promise<Role | Role[]>;
    getRolesByUser(user: User): Promise<Role[]>;
    createRole(code: string, metadata?: {[key:string]: any}, permissions?: Permission[]): Promise<Role>;
    addRoleToUser(user: User, role: Role): Promise<User>;
    addRoleToUserByCode(user: User, roleCode: string): Promise<User>;
    removeRoleFromUser(user: User, role: Role | Role[]): Promise<User>;
    deleteRole(role: Role | Role[]): Promise<void>;
}

/**
 * MODELS
 */
export declare class SessionSchema {
    static readonly TABLE_NAME: string;
}
export declare class Session extends Model {
    id: string;
    user: string;
    token: string;
    tokenTtl: number;
    refreshToken: string;
    refreshTokenTtl: number;
    device: string;
    lastRequestIp: string;
    lastRequestAt: Date;
    createdAt: Date;
    updatedAt: Date;
}
export declare class RoleSchema {
    static readonly TABLE_NAME: string;
}
export declare class Role extends Model {
    id: string;
    code: string;
    metadata: {[key:string]: any};
    permissions: string[];
    createdAt: Date;
    updatedAt: Date;
}
export declare class PermissionSchema {
    static readonly TABLE_NAME: string;
}
export declare class Permission extends Model {
    id: string;
    code: string;
    action: PermissionAction;
    path: string;
    createdAt: Date;
    updatedAt: Date;
}
export declare class UserSchema {
    static readonly TABLE_NAME: string;
}
export declare class User extends Model {
    id: string;
    username: string;
    password: string;
    email: string;
    metadata: {[key:string]: any};
    roles: string[];
    permissions: string[];
    createdAt: Date;
    updatedAt: Date;
}